# How to Contribute


## Get the source code
The simplest and most likely to succeed installation is to let GIT handle the details. If you have 
a command-line git tool on your computer, run this command: 

``` 
git clone --recurse-submodules https://gitlab.com/gis-cave/shader-toolbox-pro.git
```

The submodule recursion is critical.  The toolbox relies on a submodule (`trf`)... if you clone without the
recursion, `trf` is not included.  Nothing works without it. 

