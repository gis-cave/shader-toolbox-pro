# How to Install

## Download the latest **release**
The easiest way to get the toolbox is to download the zipp file within the
[latest release](https://gitlab.com/gis-cave/terrain-shader-toolbox/-/releases/permalink/latest)
corresponding to the GIS software you are using (ArcGIS Pro or QGIS).

[![latest release](https://gitlab.com/gis-cave/terrain-shader-toolbox/-/badges/release.svg)](https://gitlab.com/gis-cave/relief-shader-toolbox/-/releases/permalink/latest)

This will let you download a zip file that contains all the source and support files you need
to make it work. Note that the GitLab source control and packaging system includes links to a
bunch of stuff that you won't need and (likely) don't want.

![release.png](./release.png)

**Don't** download the `source code assets` (you can if you want&mdash;there's nothing secret
in there&mdash;but you will need more than just the shader source code).

Skip down to the
"Download" table. Download the appropriate zip file for your platform (QGIS or ArcGIS Pro).



## For ArcGIS Pro
### Unzip
Unzip that bundle to a stable location on your local disk. I like to put
custom toolboxes beneath the ArcGIS folder in my Documents folder:

Unzip to `C:\Users\<username>\Documents\ArcGIS\Tools` :

```text
C:\Users\<username>\Documents\ArcGIS\Tools\ReliefShaderToolbox-v1.0.0
```

Truly, it doesn't matter where it is, so long as you know where that is and it won't get
moved or deleted.

### Point ArcGIS Pro at the Toolbox Folder

Navigate to the toolbox folder using the Catalog Pane from within ArcGIS Pro. The `SurfaceShaderToolbox.pyt`
file should show up as a custom Python Toolbox within Pro. This toolbox can be opened with a
double-click  to use the tools. For easy access in future, consider adding the toolbox to either
the current project toolbox or to your favourites (right click on the toolbox to access this
capability). This will prevent you needing to find this folder in future.

## For QGIS
This shader toolbox depends on one Python module (1xarray`) which is not included when you install
QGIS... so we need to install that first before you can activate the plugin.

## Add `xarray` to the QGIS python Environment

How to do that depends on your OS.

* **Windows**
  * Launch the OSGeo4W shell<br>_This should be one of the options under the QGIS group in your start menu_
  * execute this command:
  ```
    python -m pip install xarray
  ```

* **MacOS**<br>
_NOTE_: The MacOS environment is a lot less friendly than Windows in terms of providing a shell which
mimicks the Python environment that QGIS uses. That is, MacOS version of QGIS does not offer an
equivalent to `OSGeo4W` from which to run commands. <p>There may be a few ways to go about this... here's
what I did to get `xarray` into my QGIS environment:
  * Launch QGIS
  * Open Preferences (QGIS --> Preferences --> System)
  * Add an environment variable:
    - Prepend
    - PYTHONPATH
    - /Applications/QGIS.app/Contents/Resources/python/site-packages:
  * Shut down QGIS
  * Open a Terminal and exeucte these commands:
  * ```
    export PATH=/Applications/QGIS.app/Contents/MacOS/bin/:$PATH
    cd /Applications/QGIS.app/Contents/MacOS/bin/
    ./pip install xarray

##  Load Plugin in QGIS
* Launch QGIS
* Plugins --> Manage and Install Plugins
* Select 'Install from ZIP'
* Point to the zip you downloaded from above
* You may need to restart QGIS to ensure that it loads correctly.

