


# Suited for Mac...
QGIS_PluginDir=${HOME}/Library/Application Support/QGIS/QGIS3/profiles/default/python/plugins
ArcGIS_PluginDir=${HOME}/GIS Cave/dev

PluginName=terrain-shader-toolbox

.DEFAULT: banner

banner:
	echo "Name a target"


dev: qgis-dev arcgis-dev

qgis-dev:
	rm -fr "${QGIS_PluginDir}/${PluginName}"
	mkdir -p "${QGIS_PluginDir}/${PluginName}"
	cp ./QGIS_TerrainShader/*.py "${QGIS_PluginDir}/${PluginName}"
	cp ./QGIS_TerrainShader/metadata.txt "${QGIS_PluginDir}/${PluginName}"
	cp -R ./trf "${QGIS_PluginDir}/${PluginName}"
	cp -R ./NormalMaps "${QGIS_PluginDir}/${PluginName}"
	cp -R ./SkyConfigFiles "${QGIS_PluginDir}/${PluginName}"


arcgis-dev:
	mkdir -p "${ArcGIS_PluginDir}/${PluginName}"
	cp ./ArcGIS_TerrainShader/*.pyt "${ArcGIS_PluginDir}/${PluginName}"
	cp -R ./trf "${ArcGIS_PluginDir}/${PluginName}"
	cp -R ./NormalMaps "${ArcGIS_PluginDir}/${PluginName}"
	cp -R ./SkyConfigFiles "${ArcGIS_PluginDir}/${PluginName}"

toolbox:
	rm -fr /tmp/${PluginName}
	mkdir -p /tmp/${PluginName}
	cp ./ArcGIS_TerrainShader/*.pyt "/tmp/${PluginName}"
	cp ./ArcGIS_TerrainShader/*.pyt.xml "/tmp/${PluginName}"
	cp -R ./trf "/tmp/${PluginName}"
	cp -R ./NormalMaps "/tmp/${PluginName}"
	cp -R ./SkyConfigFiles "/tmp/${PluginName}"
	cd /tmp && zip -r ./ArcGIS_${PluginName}.zip ./${PluginName}

plugin:
	rm -fr "/tmp/${PluginName}"
	mkdir -p "/tmp/${PluginName}"
	cp ./QGIS_TerrainShader/*.py "/tmp/${PluginName}"
	cp ./QGIS_TerrainShader/metadata.txt "/tmp/${PluginName}"
	cp -R ./trf "/tmp/${PluginName}"
	cp -R ./NormalMaps "/tmp/${PluginName}"
	cp -R ./SkyConfigFiles "/tmp/${PluginName}"
	cd /tmp && zip -r /tmp/QGIS_${PluginName}.zip  ${PluginName}

