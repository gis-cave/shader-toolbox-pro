The two sample normal maps included in this repository are re-scaled 
from originals obtained from ambientCG.com.

Contains assets from ambientCG.com, licensed under CC0 1.0 Universal.
