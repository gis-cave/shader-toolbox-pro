#!/usr/bin/env python

import click
import sys
import csv
import logging
import numpy as np

import xarray as xr
from osgeo import gdal
from trf import surface, shader, utils


def terrain_from_DEM(fname, elev_only=False):
    dem = gdal.Open( fname,  gdal.GA_ReadOnly)
    if dem == None:
        raise OSError("Could not load input DEM: {}".format(fname))
    srs = dem.GetSpatialRef()
    xform = dem.GetGeoTransform()
    if not srs.IsProjected():
        raise RuntimeError("Must use DEM in a projected spatial reference.")
    if dem.RasterCount != 1:
        raise RuntimeError("I expect DEM rasters made of a single band holding elevation values.")
    demData = dem.GetRasterBand(1).ReadAsArray()
    ndv = dem.GetRasterBand(1).GetNoDataValue()
    demData[demData == ndv] = 0
    if elev_only:
        terrain=xr.Dataset(
            data_vars=dict(
                DEM=(["rows", "cols"], demData),
            ),
            attrs=dict(cellwidth=xform[1])
        )
    else:
        terrain = surface.study(demData, xform[1])
    terrain.attrs['geotransform'] = xform
    terrain.attrs['spatialreference'] = srs
    dem = None
    return terrain


def write_terrain_layer( t, k, outFile):
    """
    Writes one of the xarray 'layers' to the named file

    :param outFile: Filename to write
    :param t: Terrain data structure (must be already populated)
    :param k: The key to specify which 'layer' of the terrain to export.  Example: 'SNV', 'DEM', etc.
    """
    DriverName = "GTiff"
    if not DriverName:
        DriverName = 'GTiff'
        outFile = outFile.replace('.', '_') + ".tif"
    driver = gdal.GetDriverByName(DriverName)

    try:
        x_ = t[k].data.shape[0]
        y_ = t[k].data.shape[1]
        if len(t[k].data.shape) == 3:
            z_ = t[k].data.shape[2]
        else:
            z_ = 1
    except KeyError:
        raise

    out_ds = driver.Create(outFile, y_, x_, z_, gdal.GDT_Float32)
    if out_ds == None:
        raise OSError("Could not create outfile {}".format(outFile))
    out_ds.SetGeoTransform(t.attrs['geotransform'])
    out_ds.SetProjection(t.attrs['spatialreference'].ExportToWkt())

    if z_ == 1:
        ## Single-band output
        band = out_ds.GetRasterBand(1)
        band.WriteArray(t[k].data)
    else:
        stacked_array = np.moveaxis(t[k].data, 2, 0)
        for b in range(1, z_+1):
            band = out_ds.GetRasterBand(b)
            band.WriteArray(stacked_array[b-1])
    out_ds = None


def read_lightlist(fname):
        logging.debug("Reading Sky Config File {}".format(fname))
        lts = []
        try:
            with open(fname, "r") as skyConfigFile:
                csvReadFile = csv.reader(skyConfigFile)
                for line in csvReadFile:
                    # brute force -- if this line does not have three comma-separated values, we skip it.
                    if (len(line) == 0) or (len(line) < 3):
                        continue
                    if line[0].startswith("Format"):  # special case... a comment line in the header has 2 commas in it.
                        continue
                    lts.append(line)
        except FileNotFoundError:
            logging.error("No such file {}".format(fname))
            lts = []
        except PermissionError:
            logging.error("Cannot read {}".format(fname))
            lts = []
        except IOError:
            logging.error("IO Error on file {}".format(fname))
            lts = []
        logging.debug("Found {} lights".format(len(lts)))
        return lts

def progress_bar(count, total, status=''):
    """
    Show 'thermometer bar' type progress and status on terminal

    This code adapted from https://gist.github.com/vladignatyev/06860ec2040cb497f0f3
    See also: https://gist.github.com/Minotorious/b5da8ee00301b9e9fcf3d71b92bd5973

    :param count: where we are now
    :param total: max value
    :param status: a status message string, defaults to ''
    """
    bar_len = 50
    filled_len = int(round(bar_len * count / float(total)))
    bar = '▓' * filled_len + '-' * (bar_len - filled_len)
    if count >= total:
        sys.stdout.write('\r[{}] {}/{} DONE'.format( bar, count, total))
        sys.stdout.write('\n')
    else:
        sys.stdout.write('\r[{}] {}/{}  {}'.format( bar, count, total, status))
    sys.stdout.flush()


@click.command
@click.option('-v', '--verbose', is_flag=True, default=False, help='Enables verbose mode')
@click.option('--only',          type=click.Choice(['shade', 'shadow'], case_sensitive=False))
@click.option('--lightlist',     type=click.Path(exists=True, dir_okay=False), help='Sky Config File', nargs=1, required=True)
@click.option('--ve',            type=click.FloatRange(min=0.0, max=10.0, clamp=True), help='Vertical Exageration factor', nargs=1, default=5.0)
@click.argument("infile",        type=click.Path(exists=True, dir_okay=False))
@click.argument("outfile",       type=click.Path(dir_okay=False))
def sky(verbose, only, lightlist, ve, infile, outfile):
    if verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    t = terrain_from_DEM(infile)
    t['DEM'].data *= ve
    lights = read_lightlist(lightlist)

    logging.info("Processing {} light sources ...".format(len(lights)))
    accumulator = np.zeros(t['DEM'].data.shape)

    for (i, (azimuth, elev, wt)) in enumerate(lights):
        progress_bar(i+1, len(lights))
        if only == None : # "Shade & Shadow"
            shadowArray = shader.shadowLine(t, float(azimuth), float(elev))
            shadeArray = shader.lambert(t, utils.lightVector(float(azimuth), float(elev)))
            shadeArray[shadeArray<0] = 0
            shadeArray[shadowArray>0] = 0
        if only == 'shade': # "ShadeOnly":
            shadeArray = shader.lambert(t, utils.lightVector(float(azimuth), float(elev)))
            shadeArray[shadeArray<0] = 0
        if only  == 'shadow': # "ShadowOnly":
            shadowArray = shader.shadowLine(t, float(azimuth), float(elev))
            shadowArray[shadowArray > 0] = 1
            shadeArray = 1 - shadowArray

        accumulator = accumulator + (float(wt) * shadeArray)

    t['HS'] = xr.DataArray(data=accumulator, dims=["rows", "cols"])
    write_terrain_layer(t, 'HS', outfile)

if __name__ == '__main__':
    sky()


