# Change Log

## v1.0.0

* Essential functions as a collection of GP tools in a single python toolbox.  
* Tools include:
    * Surface Normal Vectors
    * Simple Shadows
    * Smooth
    * Lambertian Shader
    * Oren-Nayer Shader
    * Lommel-Seeliger Shader
    * MDOW
    * Sky Model (Traditional)
    * Sky Model (Shadows-Only)
    * Slope Shading
    * Curvature Shading
    * TPI
    * NLCD Normal-Mapper
* Consult the [wiki](https://gitlab.com/gis-cave/shader-toolbox-pro/-/wikis/home) for background documentation on each tool.
