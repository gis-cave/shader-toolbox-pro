# -*- coding: utf-8 -*-
# License: GNU GPL v3 -- See 'LICENCE' file.

# import only the basics for now... will import more later after we get logging set up.
import logging
from logging.handlers import RotatingFileHandler
from os import path

import arcpy

# GLOBALS:
# Enabling DEBUG will alter logging behavior.
#   -> If false, only sends messages to arcpy.
#   -> If True, messages to a logfile also.
DEBUG = False

class ArcPyPassThru(logging.Handler):
    """
    Python Logging module handler to pass messages to the arcpy message queues.
    With this handler attached to a logger, you needn't call arcpy.AddMessage()
    separately -- nor should you if this is in use.
    """
    def emit(self, record):
        msg = self.format(record)
        if record.levelno >= logging.ERROR:
            arcpy.AddError(msg)
            return
        if record.levelno >= logging.WARNING:
            arcpy.AddWarning(msg)
            return
        if record.levelno >= logging.INFO:
            arcpy.AddMessage(msg)

# These need to be globals, to be used within classes, below.
arcpyHandler = ArcPyPassThru()
arcpyHandler.setFormatter( logging.Formatter("%(asctime)s >> %(message)s", datefmt='%H:%M:%S'))
logfileHandler = RotatingFileHandler(
    path.join( path.dirname(__file__), path.splitext(path.basename(__file__))[0] + ".log") ,
    maxBytes=1024*1024*2,  # 2MB log files
    backupCount=2
)
logfileHandler.setFormatter(logging.Formatter("%(levelname)-8s %(name)s.%(funcName)s %(asctime)s.%(msecs)02d %(message)s", datefmt='%H:%M:%S'))

globalLogger = logging.getLogger() #root logger
if not globalLogger.handlers:
    globalLogger.addHandler(arcpyHandler)
    if DEBUG:
        globalLogger.addHandler(logfileHandler)
        globalLogger.setLevel(logging.DEBUG)
    else:
        globalLogger.setLevel(logging.ERROR)


# OK, then.... ready to start doing stuff. . .
globalLogger.debug("Toolbox Loaded -- %s ", path.basename(__file__))
# We've deferred most of the imports until now, waiting for logging to get
# plumbed.  With DEBUG=True, you can help diagnose import problems, as those
# will not be reported to the logfile.
try:
    import numpy as np
    import xarray as xr
    from os import listdir
    from PIL import Image
    import csv
except:
    globalLogger.error("Could not import required modules.", exc_info=True)
    raise

try:
    import sys
    if path.dirname(__file__) not in sys.path:
        sys.path.append(path.dirname(__file__)) # Belt-and-suspenders....
    from trf import surface, shader, utils, bumpmap, generalize
except ImportError:
    logging.error("Could not import trf.  Be sure that your installation has included the trf sub-module.")
    raise


#  > > > > > > > > > > > > > > > TOOLBOX CONTENTS < < < < < < < < < < < < < < < <
# ==========================================================================================================
class Toolbox(object):
    """
    Toolbox implementing terrain shading methods.  Emphasizes stored surface normal vectors over height field to
    drive most of the algorithms.
    """
    def __init__(self):
        self.label = "Terrain Shader Toolbox"
        self.description = self.__doc__
        self.alias = ''.join(filter(str.isalpha, path.splitext(path.basename(__file__))[0]))
        self.tools = [  # These functions defined below...
            # Primitives
            Surface_Normal_Vectors,
            Simple_Shadows,

            # Generalization
            FFT_Smooth,
            ROF_Denoise,

            # Relief Shading
            Lambert_Shader,
            Oren_Nayer_Shader,
            Lommel_Seeliger_Shader,
            Minaert_Reflectance,

            # # Multi-Directional
            MDOW,
            Sky_Model,

            # Misc Surface Effects
            Slope_Shading,
            Curvature_Shading,
            TPI,
            AspectWeighted_Slope_Shading,

            # # Bump Mapping
            # Prep_NLCD_Normalmap_Mask,
            # NLCD_Normal_Mapper
        ]


#  > > > > > > > > > > > > > > > TOOLs  < < < < < < < < < < < < < < < <
# =============================================================================
class ReliefTool(object):
    """
    This class does not implement anything. It only exists to sub-class ....
    this boilerplate tool class handles a lot of the code which would otherwise
    be replicated in every other tool below.  Note that this tool is not listed
    in the Toolbox.tools[] list, so it will not be seen by the ArcGIS user.
    """
    _suffix = "_xxx" #<-- placeholder.. override in subclasses.
    # See update_parameters() to see how this is used.

    def __init__(self, **kwargs):
        fname = path.splitext(path.basename(__file__))[0]
        if 'toolname' in kwargs:
            self.label = kwargs.get('toolname').replace('_', ' ')
            self.LOG = logging.getLogger(kwargs.get('toolname'))
        else:
            self.label = __class__.__name__.replace('_', ' ')
            self.LOG = logging.getLogger(__class__.__name__)
        self.LOG.propagate = False
        if not self.LOG.handlers:
            self.LOG.addHandler(arcpyHandler)
            if DEBUG:
                self.LOG.addHandler(logfileHandler)
                self.LOG.setLevel(logging.DEBUG)
            else:
                self.LOG.setLevel(logging.INFO)
        # call self.LOG.<LEVEL>(message) and have the message go to the right
        # place.  Use self.LOG instead of the global logger so that messages
        # get stamped with the class name.

        self.description = self.__doc__
        #    ^^^^ We set this here to ensure that it has a value. It should be
        #         overridden in the subclass to be that class's doc string.

    def getParameterInfo(self):
        """
        These four parameters are fairly standard for any relief shading tool
        in this toolbox. The subclassed tools can call super().getParameterInfo()
        (i.e. this method) to get the boilerplate parameter list, then adjust.
        Note the order, as it will be important later.  If you are going to ask
        for different parameters than just these four, the first line of the
        subclassed tool should be:
          [inputDEM, lightAzimuth, lightElev, outputRaster] = super().getParameterInfo()
        """
        inputDEM = arcpy.Parameter(
            displayName="Elevation Raster",
            name='input',
            datatype=["GPRasterLayer"],
            parameterType="Required",
            direction="Input"
        )
        lightAzimuth = arcpy.Parameter(
            displayName="Light Azimuth",
            name="lightAz",
            datatype="GPDouble",
            parameterType="Required",
            direction="Input"
        )
        lightAzimuth.value = 337.5
        # Biland, Julien & Coltekin, Arzu. (2016). DOI:10.1080/15230406.2016.1185647

        lightElev = arcpy.Parameter(
            displayName="Light Elevation",
            name="lightEl",
            datatype="GPDouble",
            parameterType="Required",
            direction="Output"
        )
        lightElev.value = 45

        outputRaster = arcpy.Parameter(
            displayName="Output",
            name="output",
            datatype="DERasterDataset",
            parameterType="Required",
            direction="Output"
        )
        return [inputDEM, lightAzimuth, lightElev, outputRaster]

    def isLicensed(self):
        return True

    def updateParameters(self, parameters):
        # ASSUMES that parameter with index=0 is the input DEM
        if parameters[0].valueAsText:
            # the index of the output parameter differs by tool... we're
            # looking for an output param with key 'output'.  The list should be short,
            # so a linear search should be fine.
            for i in range(0, len(parameters)):
                if parameters[i].name == 'output':
                    break
            if not parameters[i].altered:
                # Sets a useful default for the output path/file.. the _suffix string is put at
                # the end of the input path name as a first guess.  User can always override.
                parameters[i].value = path.join(
                    arcpy.env.workspace,
                    path.splitext(path.basename(parameters[0].valueAsText))[0] + self._suffix
                )
        return

    def updateMessages(self, parameters):
        return

    def execute(self, parameters, messages):
        if DEBUG:
            arcpy.AddMessage("DEBUG logfile available in {}".format(logfileHandler.baseFilename))
        return

    def args(self, params):
        """
        Takes the parameter list and makes a dict out of it.
        """
        # This could, in theory, be done as a single line using a list comprehension expression
        # instead of calling this function. I chose to do it 'longhand' this way so that I can
        # insert logging calls to report the parameters to the debugging logger.
        argv = {}
        for p in params:
            self.LOG.debug("PARAM {} = {}".format(p.name, p.valueAsText))
            argv[p.name] = p  # convenience dict
        return argv

    def read_input_DEM(self, inputParam, elev_only=False):
        inputDEM = inputParam.valueAsText
        self.statusUpdate("Reading DEM: {}".format(path.basename(inputDEM)))
        demInfo = arcpy.da.Describe(inputParam.value)
        arcpy.env.outputCoordinateSystem = demInfo['spatialReference']
        heightField = arcpy.RasterToNumPyArray(inputParam.valueAsText, nodata_to_value=0).astype(np.float64)

        # if elev_only, no need to compute SNV... just pack the ['DEM'] DataArray
        if elev_only:
            self.LOG.debug("elev_only specified -- skipping SNV calculations.")
            terrain=xr.Dataset(
                data_vars=dict(
                    DEM=(["rows", "cols"], heightField),
                ),
                attrs=dict(cellwidth=demInfo['meanCellWidth'])
            )
        else:
            self.statusUpdate("Computing surface normal vectors...")
            terrain = surface.study(
                heightField,
                demInfo['meanCellWidth']
                )
        terrain.attrs['demInfo'] = demInfo
        return terrain

    def write_output_raster(self, outRaster, outputParam, crsInfo):
        try:
            self.statusUpdate("Writing Output to {} ...".format(
                path.basename(outputParam.valueAsText)
            ))
            outRaster = arcpy.NumPyArrayToRaster(outRaster,
                arcpy.Point(
                    crsInfo['extent'].XMin, crsInfo['extent'].YMin
                ),
                crsInfo['meanCellWidth'],
                crsInfo['meanCellHeight']
            )
            outRaster.save(outputParam.valueAsText)
            self.LOG.debug(r"Successfully written to {outputParam.valueAsText}")
        except:
            self.LOG.exception("Unable to save output.",  exc_info=True)
            raise
        return


    def statusUpdate(self, msg):
        """
        Convenience function to bundle messaging -- sets the 'progressor' message in the GUI
        and also invokes the logger attached to this class.

        :param msg: Pre-formatted message to publish.
        """
        arcpy.SetProgressor("default", msg)
        self.LOG.info(msg)
        return



class Surface_Normal_Vectors(ReliefTool):
    """
    Computes surface normal vectors of a terrain, given its height field.

    The output is a 3-band raster of the x, y, and z components of the surface normals.
    """
    _suffix = "_SNV"

    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.description = self.__doc__
        self.category = "Primitives"

    def getParameterInfo(self):
        [inputDEM, _, _, outputRaster] = super().getParameterInfo()
        return [inputDEM, outputRaster]


    def execute(self, parameters, messages):
        super().execute(parameters, messages)
        argv = self.args(parameters)

        terrain = self.read_input_DEM(argv['input'])

        ## https://gis.dhec.sc.gov/image/help/en/notebook/latest/python/windows/numpyarraytoraster-function.htm
        # arcpy.NumPyArrayToRaster will write a multi-band raster output, but the band must be the first index.
        # The convention elsewhere (from the image world) is that the band is the last index.  So we have to
        # reshape for arcpy's benefit.
        snv = np.moveaxis(terrain['SNV'].data, 2, 0)
        self.write_output_raster(snv, argv['output'], terrain.attrs['demInfo'])
        return


class Simple_Shadows(ReliefTool):
    """
    Simple shadow caster...  This is a line-of-site tool, not dependent on the surface normals.
    Output array includes pixels with value=1 for enshadowed area, nodata otherwise.

    If 'Soften Shadow Edges' is true, data values indicate depth of shadow (0=weak shadow; 1=dark shadow)
    with nodata still being used to indicate no shadow at all.

    The 'soft' shadow runs what is effectively a limited sky_model with light directions being varied +/-
    the specified azimuth. The effect is to dither the edges of the shadows somewhat.
    """
    _suffix = "_SHADOW"
    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.description = self.__doc__
        self.category = "Primitives"

    def execute(self, parameters, messages):
        super().execute(parameters, messages)
        argv = self.args(parameters)

        terrain = self.read_input_DEM(argv['input'], elev_only=True)

        self.statusUpdate("Casting Shadows...")
        shadowArray = shader.shadowLine(terrain, argv['lightAz'].value, argv['lightEl'].value)
        shadowArray[shadowArray > 0] = 1

        ## So the output raster will only contain data for shadowed areas.
        shadowArray[shadowArray == 0] = np.nan

        self.write_output_raster(shadowArray, argv['output'], terrain.attrs['demInfo'])
        return


class FFT_Smooth(ReliefTool):
    """
    Generalizes/smooths terrain using Fast Fourier Transforms.
    """
    _suffix = "_Smoothed"
    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.description = self.__doc__
        self.category = "Generalize"

    def getParameterInfo(self):
        [inputDEM, _, _, outputRaster] = super().getParameterInfo()
        kernel_shape = arcpy.Parameter(
            displayName="Smoothing Kernel Shape",
            name="shape",
            datatype="GPString",
            parameterType="Required",
            direction="Input"
        )
        kernel_shape.filter.list = [
            "Box",
            "Circle",
            "Gaussian"
            ]
        kernel_shape.value = "Box"

        span = arcpy.Parameter(
            displayName="Span",
            name="span",
            datatype="GPLong",
            parameterType="Required",
            direction="Input"
        )
        span.value = 9
        return [inputDEM, kernel_shape, span, outputRaster]


    def execute(self, parameters, messages):
        super().execute(parameters, messages)
        argv = self.args(parameters)

        terrain = self.read_input_DEM(argv['input'], elev_only=True)

        try:
            size = int(argv['span'].value)
            shp = argv['shape'].valueAsText
            self.statusUpdate("Smoothing raster with FFT... ")
            smoothed_terrain = generalize.fft_smooth(terrain['DEM'].data, size, shape=shp)
        except RuntimeError:
            self.LOG.error("Error.", exc_info=True)
            return

        self.write_output_raster(smoothed_terrain, argv['output'], terrain.attrs['demInfo'])
        return


class ROF_Denoise(ReliefTool):
    """
    """
    _suffix = "_ROF"
    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.description = self.__doc__
        self.category = "Generalize"

    def getParameterInfo(self):
        [inputDEM, _, _, outputRaster] = super().getParameterInfo()

        weight = arcpy.Parameter(
            displayName="Weight",
            name="weight",
            datatype="GPLong",
            parameterType="Required",
            direction="Input"
        )
        weight.value = 20
        return [inputDEM, weight, outputRaster]


    def execute(self, parameters, messages):
        super().execute(parameters, messages)
        argv = self.args(parameters)

        terrain = self.read_input_DEM(argv['input'], elev_only=True)

        try:
            wt = int(argv['weight'].value)
            self.statusUpdate("Smoothing raster with ROF Denoiser... ")
            (denoised_dem, _ ) = generalize.rof_denoise(terrain['DEM'].data, tv_weight=wt)
        except RuntimeError:
            self.LOG.error("Error.", exc_info=True)
            return
        self.write_output_raster(denoised_dem, argv['output'], terrain.attrs['demInfo'])
        return



## Surface shaders ---
class Lambert_Shader(ReliefTool):
    """
    Produces shaded output per Lambert's Cosine Emission Law.  Output range
    can be manipulated according to the eventual use of the shades.
    """
    _suffix = "_HS"
    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.description = self.__doc__
        self.category = "Shaded Relief"

    def getParameterInfo(self):
        [inputDEM, lightAzimuth, lightElev, outputRaster] = super().getParameterInfo()
        output_options = arcpy.Parameter(
            displayName="Output Options:",
            name='outopt',
            datatype="GPString",
            parameterType="Optional",
            direction="Input"
        )
        output_options.value = "Traditional"
        output_options.filter.list = [
            "Simple Lambert",
            "Traditional",
            "Clamped",
            "Soft Shade",
            "Half Lambert",
            ]
        return [inputDEM, lightAzimuth, lightElev, output_options, outputRaster]

    def execute(self, parameters, messages):
        super().execute(parameters, messages)
        argv = self.args(parameters)
        terrain = self.read_input_DEM(argv['input'])

        self.statusUpdate("Calculating Surface Shade Values...")
        L = utils.lightVector(argv['lightAz'].value, argv['lightEl'].value)
        hs = shader.lambert(terrain, L)  # lambert output is -1 to 1...

        if "Clamped" in argv['outopt'].valueAsText:
            self.LOG.debug("CLAMPING output to [0..1]")
            hs = utils.clamp(hs, bounds=(0,1)) # Chop off values below zero --> set to zero

        if "Traditional" in argv['outopt'].valueAsText:
            self.LOG.debug("Clamping and scaling to [0..255")
            hs = utils.clamp(hs, bounds=(0,1)) * 255.0
            hs = hs.astype('uint8')

        if "Soft" in argv['outopt'].valueAsText:
            self.LOG.debug("Soft Shade -- coercing output to [0..1]")
            hs = (hs + 1) / 2  # rescale to fit 0 to 1 without loss of data/detail

        if "Half" in argv['outopt'].valueAsText:
            self.LOG.debug('SQUARING output (half-lambert) to [0..1]')
            hs = ((hs + 1) / 2)**2  # similar to "coerce", but the output curve is affected by the exponent.

        self.write_output_raster(hs, argv['output'], terrain.attrs['demInfo'])
        return


class Oren_Nayer_Shader(ReliefTool):
    """
    Produces shaded output with BVs in range 0 to 1.  values below 0.5 are self-shaded.
    This is essentially the output from lambert, re-scaled to fit into the range [0 .. 1] rather than the
    cosine output of [-1 .. 0].
    """
    _suffix = "OrNay"

    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.description = self.__doc__
        self.category = "Shaded Relief"

    def getParameterInfo(self):
        [inputDEM, lightAzimuth, lightElev, outputRaster] = super().getParameterInfo()
        sigma = arcpy.Parameter(
            displayName="Sigma",
            name='sigma',
            datatype="GPDouble",
            parameterType="Required",
            direction="Input"
        )
        sigma.value = 0.2
        return [inputDEM, lightAzimuth, lightElev, sigma, outputRaster]

    def execute(self, parameters, messages):
        super().execute(parameters, messages)
        argv = self.args(parameters)
        terrain = self.read_input_DEM(argv['input'])

        # Ready to do work
        arcpy.SetProgressor("default", "Calculating Surface Shade Values...")
        L = utils.lightVector(argv['lightAz'].value, argv['lightEl'].value)
        hs = shader.oren_nayer(terrain, L, argv['sigma'].value)

        self.write_output_raster(hs, argv['output'], terrain.attrs['demInfo'])
        return


class Lommel_Seeliger_Shader(ReliefTool):
    """
    Produces shaded output with BVs in range 0 to 1.  values below 0.5 are self-shaded.
    This is essentially the output from lambert, re-scaled to fit into the range [0 .. 1] rather than the
    cosine output of [-1 .. 0].
    """
    _suffix = "LomSel"
    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.description = self.__doc__
        self.category = "Shaded Relief"

    def execute(self, parameters, messages):
        super().execute(parameters, messages)
        argv = self.args(parameters)
        terrain = self.read_input_DEM(argv['input'])

        arcpy.SetProgressor("default", "Calculating Surface Shade Values...")
        L = utils.lightVector(argv['lightAz'].value, argv['lightEl'].value)
        hs = shader.lommel_seeliger(terrain, L)

        self.write_output_raster(hs, argv['output'], terrain.attrs['demInfo'])
        return



class Minaert_Reflectance(ReliefTool):
    """
    """
    _suffix = "_MR"

    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.description = self.__doc__
        self.category = "Shaded Relief"

    def getParameterInfo(self):
        [inputDEM, lightAzimuth, lightElev, outputRaster] = super().getParameterInfo()
        K = arcpy.Parameter(
            displayName="K",
            name='K',
            datatype="GPDouble",
            parameterType="Required",
            direction="Input"
        )
        K.value = 0.8
        return [inputDEM, lightAzimuth, lightElev, K, outputRaster]

    def execute(self, parameters, messages):
        super().execute(parameters, messages)
        argv = self.args(parameters)
        terrain = self.read_input_DEM(argv['input'])

        hs = shader.minnaerts_reflectance(
            terrain,
            argv['K'].value,
            utils.lightVector(argv['lightAz'].value, argv['lightEl'].value)
            )
        self.write_output_raster(hs, argv['output'], terrain.attrs['demInfo'])
        return



class Slope_Shading(ReliefTool):
    """
    Computes the slope in degrees, based on  normal vectors of a terrain.
    """
    _suffix = "_SLP"
    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.description = self.__doc__
        self.category = "Surface Effects"

    def getParameterInfo(self):
        [inputDEM, _, _, outputRaster] = super().getParameterInfo()
        return [inputDEM, outputRaster]

    def execute(self, parameters, messages):
        super().execute(parameters, messages)
        argv = self.args(parameters)
        terrain = self.read_input_DEM(argv['input'])

        try:
            slp = terrain['SNV'].sel(component="Nz").data
        except RuntimeError:
            self.LOG.error("Error calculating slope.")
            return
        self.write_output_raster(slp, argv['output'], terrain.attrs['demInfo'])
        return


class Curvature_Shading(ReliefTool):
    """
    Computes the profile curvature.
    """
    _suffix = "_CURV"
    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.description = self.__doc__
        self.category = "Surface Effects"

    def getParameterInfo(self):
        [inputDEM, _, _, outputRaster] = super().getParameterInfo()
        span = arcpy.Parameter(
            displayName="Span",
            name="span",
            datatype="GPLong",
            parameterType="Required",
            direction="Input"
        )
        span.value = 1
        return [inputDEM, span, outputRaster]

    def execute(self, parameters, messages):
        super().execute(parameters, messages)
        argv = self.args(parameters)
        terrain= self.read_input_DEM(argv['input'], elev_only=True)

        try:
            _k = surface.profile_curvature(terrain, span=int(argv['span'].value))
            n = np.absolute(_k).max()
        except RuntimeError:
            self.LOG.error("Error calculating curvature.")
            return
        self.write_output_raster(_k/n, argv['output'], terrain.attrs['demInfo'])
        return


class TPI(ReliefTool):
    """
    Terrain Position Index
    """
    _suffix = "_TPI"
    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.description = self.__doc__
        self.category = "Surface Effects"

    def getParameterInfo(self):
        [inputDEM, _, _, outputRaster] = super().getParameterInfo()
        kernel_shape = arcpy.Parameter(
            displayName="Smoothing Kernel Shape",
            name="shape",
            datatype="GPString",
            parameterType="Required",
            direction="Input"
        )
        kernel_shape.filter.list = [
            "Box",
            "Circle",
            "Gaussian",
            ]
        kernel_shape.value = "Box"


        span = arcpy.Parameter(
            displayName="Span",
            name="span",
            datatype="GPLong",
            parameterType="Required",
            direction="Input"
        )
        span.value = 3
        return [inputDEM, kernel_shape, span, outputRaster]

    def execute(self, parameters, messages):
        super().execute(parameters, messages)
        argv = self.args(parameters)

        terrain = self.read_input_DEM(argv['input'], elev_only=True)

        try:
            size = int(argv['span'].value)
            shp = argv['shape'].valueAsText

            _tpi = terrain["DEM"].data - generalize.fft_smooth(terrain['DEM'].data, size, shape=shp)
            n = np.absolute(_tpi).max() # normalize to get range from [-1..1]

        except RuntimeError:
            self.LOG.error("Error calculating curvature.")
            return

        self.write_output_raster(_tpi/n , argv['output'], terrain.attrs['demInfo'])

        return


class AspectWeighted_Slope_Shading(ReliefTool):
    """
    """
    _suffix = "_AWSLP"
    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.description = self.__doc__
        self.category = "Surface Effects"

    def getParameterInfo(self):
        [inputDEM, az, _, outputRaster] = super().getParameterInfo()
        return [inputDEM, az, outputRaster]

    def execute(self, parameters, messages):
        super().execute(parameters, messages)
        argv = self.args(parameters)
        terrain = self.read_input_DEM(argv['input'])

        try:
            weights = shader.aspect_shading(terrain, utils.lightVector(argv['lightAz'].value , 0))
            # weights are 0 (typically black) for areas facing away from light; 1 (white) for areas facing the light.
            # We need that reversed for the multiplication step below (0=white  :=  multiply by zero to remove/reduce)
            w = 1-weights
            slp = terrain['SNV'].sel(component="Nz").data
            # Same deal with slopes -- we need darkly shaded areas to have high value so that white areas multiply by zero
            s = 1-slp
            results = 1 - (w * s) # Need to undo, so that 0=dark again
            results[np.isnan(results)] = 1
        except RuntimeError:
            self.LOG.error("Error calculating slope.")
            return
        self.write_output_raster(results, argv['output'], terrain.attrs['demInfo'])
        return


class MDOW(ReliefTool):
    """
    Multi-Directional Oblique Weighted Hillshade
    """
    _suffix = "_MDOW"
    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.description = self.__doc__
        self.category = "Multi-Directional"

    def getParameterInfo(self):
        [inputDEM, _, _, outputRaster] = super().getParameterInfo()
        return [inputDEM, outputRaster]

    def execute(self, parameters, messages):
        super().execute(parameters, messages)
        argv = self.args(parameters)

        t = self.read_input_DEM(argv['input'])

        _center=295.5
        _step=22.5

        self.statusUpdate("Computing hillshades ...")

        shade225 = shader.clamp(shader.lambert(t, utils.lightVector(_center - 3*_step, 30)))
        shade270 = shader.clamp(shader.lambert(t, utils.lightVector(_center - _step, 30)))
        shade315 = shader.clamp(shader.lambert(t, utils.lightVector(_center + _step, 30)))
        shade360 = shader.clamp(shader.lambert(t, utils.lightVector(_center + 3*_step, 30)))

        self.statusUpdate("Smoothing Terrain ...")
        h03 = generalize.fft_smooth(t['DEM'].data, 30, shape="Circle")

        self.statusUpdate("Computing Aspect of Smoothed Terrain ...")
        h03_t = surface.study(h03, t.attrs['demInfo']['meanCellWidth'])
        asp = surface.aspect(h03_t)
        asp[asp < 0] = 293

        self.statusUpdate("Computing Weight Factors ...")
        w225 = np.sin( (asp - (_center - 3*_step)) * (np.pi / 180) )**2
        w270 = np.sin( (asp - (_center - 1*_step)) * (np.pi / 180) )**2
        w315 = np.sin( (asp - (_center + _step)) * (np.pi / 180) )**2
        w360 = np.sin( (asp - (_center + 3*_step)) * (np.pi / 180) )**2

        self.statusUpdate("Assembling final hillshade...")
        hs = (w225*shade225  + w270*shade270 + w315*shade315 + w360*shade360) / 2

        self.write_output_raster(hs, argv['output'], t.attrs['demInfo'])
        return


class Sky_Tool(ReliefTool):
    """
    This is an abstract class to set some common methods for the various
    sky model methods.  Sky Model is a variation of a technique published here:
    Kennelly, P. J., & Stewart, A. J. (2014). https://doi.org/10.1080/13658816.2013.848985
    """
    def __init__(self, **kwargs):
        if 'toolname' in kwargs:
            super().__init__(toolname=kwargs.get('toolname'))
        else:
            super().__init__(toolname=__class__.__name__)
        self.description = self.__doc__

    def getParameterInfo(self):
        [inputDEM, _, _, outputRaster] = super().getParameterInfo()
        skyviewConfigFile = arcpy.Parameter(
            displayName="Sky Configuration File:",
            name='config',
            datatype="GPString",
            parameterType="Required",
            direction="Input"
        )
        try:
            dname = path.join(path.dirname(__file__), "SkyConfigFiles")
            self.LOG.debug("Looking for sky configs in {}... ".format(dname))
            skyviewConfigFile.filter.list = sorted([f for f in listdir(dname) if f.endswith('.txt')])
            self.LOG.debug("...found {}".format(len(skyviewConfigFile.filter.list)))
        except Exception:
            self.LOG.info("ERROR trying to get list of sky config files.", exc_info=True)
            raise

        shadowLength = arcpy.Parameter(
            displayName="Shadow-Length",
            name='ShadowLength',
            datatype="GPDouble",
            parameterType="Required",
            direction="Input"
        )
        shadowLength.value = 5
        shadowLength.filter.list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        return [inputDEM, skyviewConfigFile, shadowLength, outputRaster]

    def lightlist_from_file(self, f):
        self.LOG.info("Reading Sky Config File: {}".format(f))
        lts = []
        try:
            with open(f, "r") as skyConfigFile:
                csvReadFile = csv.reader(skyConfigFile)
                for line in csvReadFile:
                    # brute force -- if this line does not have three comma-separated values, we skip it.
                    if (len(line) == 0) or (len(line) < 3):
                        continue
                    if line[0].startswith("Format"):  # special case... a comment line in the header has 2 commas in it.
                        continue
                    lts.append(line)
        except Exception:
            self.LOG.info("ERROR trying to read lighting config from {}.".format(f), exc_info=True)
            raise
        self.LOG.debug("{} lights found.".format(len(lts)))
        return lts


class Sky_Model(Sky_Tool):
    """

    """
    _suffix = "_SKY"
    def __init__(self, **kwargs):
        super().__init__(toolname=__class__.__name__)
        self.description = self.__doc__
        self.category = "Multi-Directional"

    def getParameterInfo(self):
        [inputDEM, skyviewConfigFile, shadowLength, outputRaster] = super().getParameterInfo()

        layer_combo = arcpy.Parameter(
            displayName="Hillshade Options",
            name='hs_opt',
            datatype="GPString",
            parameterType="Optional",
            direction="Input"
        )
        layer_combo.value = "Shade & Shadow"
        layer_combo.filter.list = [
            "Shade & Shadow",
            "Shade Only",
            "Shadow Only",
            ]
        return [inputDEM, skyviewConfigFile, shadowLength, layer_combo, outputRaster]


    def execute(self, parameters, messages):
        super().execute(parameters, messages)
        argv = self.args(parameters)
        terrain = self.read_input_DEM(argv['input'])

        # After surface normals are calculated, it is safe to tweak the DEM.
        # Multiply heights by a VE factor to increase shadow length.
        # Because surface normals are not affected, VE does not impact shading - only shadow.
        mn = terrain['DEM'].data.min()
        mx = terrain['DEM'].data.max()
        terrain['DEM'].data *= argv['ShadowLength'].value
        self.LOG.debug("Applying VE of {}; Range of elevations changed from [{} .. {}] to [{} .. {}]".format(
            argv['ShadowLength'].value,
            mn, mx,
            terrain['DEM'].data.min(), terrain['DEM'].data.max()
            )
        )

        confFile = path.join(path.dirname(__file__) , "SkyConfigFiles" , argv['config'].valueAsText)
        lights = self.lightlist_from_file(confFile)
        # OK... after all that, let's start shading (FINALLY !). . .
        accumulator = np.zeros(terrain['DEM'].shape)
        arcpy.SetProgressor("step", f"Processing {len(lights)} Illumination Sources...", 0, len(lights), 1)
        for (i, (azimuth, elev, wt)) in enumerate(lights):
            arcpy.SetProgressorLabel(f"Working on light {i+1} of {len(lights)}:  Az={azimuth}, Elev={elev}")
            arcpy.SetProgressorPosition(i)
            L = utils.lightVector(float(azimuth), float(elev))
            if argv['hs_opt'].valueAsText == "Shade & Shadow":
                shadowArray = shader.shadowLine(terrain, float(azimuth), float(elev))
                shadeArray = shader.lambert(terrain, utils.lightVector(float(azimuth), float(elev)))
                shadeArray[shadeArray<0] = 0
                shadeArray[shadowArray>0] = 0

            if argv['hs_opt'].valueAsText == "Shade Only":
                shadeArray = shader.lambert(terrain, utils.lightVector(float(azimuth), float(elev)))
                shadeArray[shadeArray<0] = 0

            if argv['hs_opt'].valueAsText == "Shadow Only":
                shadowArray = shader.shadowLine(terrain, float(azimuth), float(elev))
                shadowArray[shadowArray > 0] = 1
                shadeArray = 1 - shadowArray

            accumulator = accumulator + (float(wt) * shadeArray)

        self.statusUpdate("Processed {} light sources.".format(len(lights)))
        self.write_output_raster(accumulator, argv['output'], terrain.attrs['demInfo'])
        return

