# Terrain Shader Toolbox

This is a python toolbox built to bring some terrain shading functions into one
place for easy use.  This code base supports plugins/toolboxes for both QGIS and
for ArcGIS Pro. It is a revision and extension of a prior project built as part 
of my final MGIS project.  See the references below. 


## Install
See [INSTALL.md](./INSTALL.md) for instructions on how to install this toolbox.


## Toolbox Contents
The python toolbox contains several tools.  The 
[WIKI](https://gitlab.com/gis-cave/terrain-shader-toolbox/-/wikis/home) documentation
covers each tool and some details about how it works.


## References and Inspiration

* This project is inspired by and is loosely organized similar to the excellent
  [Terrain Tools](https://www.arcgis.com/home/item.html?id=4b2ea7c5f87d476a8849c804b81667aa)
  software bundle from Ken Field and his Esri colleagues. 
* The original MGIS project and the paper it produced is available at:<br>
  Trantham, G. &amp; Kennelly, P. (2022) Terrain representation using 
  orientation. _Cartography and Geographic Information Science_.<br>
  DOI: [10.1080/15230406.2022.2035256](https://doi.org/10.1080/15230406.2022.2035256)
