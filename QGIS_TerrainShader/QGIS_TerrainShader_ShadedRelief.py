# -*- coding: utf-8 -*-

# from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterRasterDestination,
                       QgsProcessingParameterEnum, QgsProcessingParameterNumber,
                       QgsProcessingException, QgsProcessingFeedback
                       )


from .QGIS_TerrainShader_BaseClasses import Relief_Tool_Algorithm

from .trf import shader, utils
import xarray as xr


class Lambertian_Shader_Algorithm(Relief_Tool_Algorithm):
    """
    Computes surface relfectance based on Lambert cosine emission law.  Lambert's natural output of [-1 .. 1]
    is post-processed using the specified method to ensure that the output is constrained to [0 .. 1].

    •Clamped output sets all negative values to zero.
    •Soft-shaded output applies an affine transform to scale and move the output curve.
    •Half-Lambert output applies an exponent to alter the output distribution curve shape.

    Default is 'Clamp'
    """
    POST_PROCESS = "POST_PROCESS"
    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.__helpstr__ = self.__doc__
        self.__group__ = "Shaded Relief"

    def initAlgorithm(self, config):
        super().initAlgorithm(config)
        self.addParameter(
            QgsProcessingParameterEnum(
                self.POST_PROCESS,
                description="Post-processing method:",
                defaultValue="Clamped",
                options= [ "Simple Lambert", "Traditional", "Clamped", "Soft Shade", "Half Lambert"]
            )
        )
        return


    def processAlgorithm(self, parameters, context, feedback):
        if feedback is None:
            feedback = QgsProcessingFeedback()
        try:
            dem_path = str(self.parameterAsRasterLayer(parameters, self.INPUT, context).source())
            feedback.pushInfo("Reading {} ...".format(dem_path))
            t = self.terrain_from_DEM(dem_path)
        except RuntimeError as e:
            feedback.reportError("It is pitch black; you are likely to be eaten by a grue.\n", fatalError=True)
            raise QgsProcessingException(e)

        az = float(self.parameterAsDouble(parameters, self.AZ, context ))
        el = float(self.parameterAsDouble(parameters, self.EL, context ))
        _lambert = shader.lambert(t, utils.lightVector(az, el))
        _pp = int(self.parameterAsString(parameters, self.POST_PROCESS, context))
        if (_pp == 0):
            hs = _lambert
        if (_pp == 2 or _pp == 1):
            # clamped and traditional are synonyms
            hs = shader.clamp(_lambert)
        if (_pp == 3):
            hs = shader.soft(_lambert)
        if (_pp == 4):
            hs = shader.half(_lambert)
        del _lambert

        t['HS'] = xr.DataArray(data=hs, dims=["rows", "cols"])

        output_path = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        self.write_terrain_layer(t, 'HS', output_path)
        return {self.OUTPUT: output_path}

    def createInstance(self):
        return self.__class__()


class LommelSeeliger_Shader_Algorithm(Relief_Tool_Algorithm):
    """
    Computes  Lommel-Seeliger reflectance.
    """
    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.__helpstr__ = self.__doc__
        self.__group__ = "Shaded Relief"

    def processAlgorithm(self, parameters, context, feedback):
        if feedback is None:
            feedback = QgsProcessingFeedback()
        try:
            dem_path = str(self.parameterAsRasterLayer(parameters, self.INPUT, context).source())
            feedback.pushInfo("Reading {} ...".format(dem_path))
            t = self.terrain_from_DEM(dem_path)
        except RuntimeError as e:
            feedback.reportError("It is pitch black; you are likely to be eaten by a grue.\n", fatalError=True)
            raise QgsProcessingException(e)

        az = float(self.parameterAsDouble(parameters, self.AZ, context ))
        el = float(self.parameterAsDouble(parameters, self.EL, context ))

        hs = shader.lommel_seeliger(t, utils.lightVector(az, el))
        t['HS'] = xr.DataArray(data=hs, dims=["rows", "cols"])

        output_path = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        self.write_terrain_layer(t, 'HS', output_path)
        return {self.OUTPUT: output_path}

    def createInstance(self):
        return self.__class__()


class OrenNayer_Shader_Algorithm(Relief_Tool_Algorithm):
    """
    Computes  Oren-Nayer reflectance.

    Sigma is a measure of 'roughness' for the surface. Sigma=0 is equivalent to Lambertian (un-roughened) model.
    """
    SIGMA="SIGMA"

    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.__helpstr__ = self.__doc__
        self.__group__ = "Shaded Relief"

    def initAlgorithm(self, config):
        super().initAlgorithm(config)
        self.addParameter(
            QgsProcessingParameterNumber(
                self.SIGMA,
                description="Sigma",
                type=QgsProcessingParameterNumber.Double,
                defaultValue=0.2,
                minValue=0.0,
                maxValue=5
            )
        )


    def processAlgorithm(self, parameters, context, feedback):
        if feedback is None:
            feedback = QgsProcessingFeedback()
        try:
            dem_path = str(self.parameterAsRasterLayer(parameters, self.INPUT, context).source())
            feedback.pushInfo("Reading {} ...".format(dem_path))
            t = self.terrain_from_DEM(dem_path)
        except RuntimeError as e:
            feedback.reportError("It is pitch black; you are likely to be eaten by a grue.\n", fatalError=True)
            raise QgsProcessingException(e)

        az = float(self.parameterAsDouble(parameters, self.AZ, context ))
        el = float(self.parameterAsDouble(parameters, self.EL, context ))
        sigma = float(self.parameterAsDouble(parameters, self.SIGMA, context))
        hs = shader.oren_nayer(t, utils.lightVector(az, el), sigma)
        t['HS'] = xr.DataArray(data=hs, dims=["rows", "cols"])

        output_path = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        self.write_terrain_layer(t, 'HS', output_path)
        return {self.OUTPUT: output_path}

    def createInstance(self):
        return self.__class__()


class Minnaerts_Reflectance_Algorithm(Relief_Tool_Algorithm):
    """
    """
    K="K"

    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.__helpstr__ = self.__doc__
        self.__group__ = "Shaded Relief"

    def initAlgorithm(self, config):
        super().initAlgorithm(config)
        self.addParameter(
            QgsProcessingParameterNumber(
                self.K,
                description="K",
                type=QgsProcessingParameterNumber.Double,
                defaultValue=0.8,
                minValue=0.0,
                maxValue=1.0
            )
        )


    def processAlgorithm(self, parameters, context, feedback):
        if feedback is None:
            feedback = QgsProcessingFeedback()
        try:
            dem_path = str(self.parameterAsRasterLayer(parameters, self.INPUT, context).source())
            feedback.pushInfo("Reading {} ...".format(dem_path))
            t = self.terrain_from_DEM(dem_path)
        except RuntimeError as e:
            feedback.reportError("It is pitch black; you are likely to be eaten by a grue.\n", fatalError=True)
            raise QgsProcessingException(e)

        az = float(self.parameterAsDouble(parameters, self.AZ, context ))
        el = float(self.parameterAsDouble(parameters, self.EL, context ))
        k = float(self.parameterAsDouble(parameters, self.K, context))

        hs = shader.minnaerts_reflectance(t, k, utils.lightVector(az, el))

        t['HS'] = xr.DataArray(data=hs, dims=["rows", "cols"])

        output_path = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        self.write_terrain_layer(t, 'HS', output_path)
        return {self.OUTPUT: output_path}

    def createInstance(self):
        return self.__class__()