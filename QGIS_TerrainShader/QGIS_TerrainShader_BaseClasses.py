# -*- coding: utf-8 -*-

__author__ = 'GIS Cave'
__date__ = '2022-07-01'
__copyright__ = '(C) 2022 by GIS Cave'


from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (Qgis,
                        QgsMessageLog,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterRasterDestination,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterEnum,
                       QgsRasterFileWriter
                       )

import csv
import os
import inspect
import numpy as np
import xarray as xr
from osgeo import gdal
from trf import surface

class Relief_Tool_Algorithm(QgsProcessingAlgorithm):
    OUTPUT = 'OUTPUT'
    INPUT = 'INPUT'
    VE = 'VE'
    AZ = 'AZ'
    EL = 'EL'

    def createInstance(self):
        return Relief_Tool_Algorithm()

    def __init__(self, **kwargs) -> None:
        super().__init__()
        if 'toolname' in kwargs:
            _label = kwargs.get('toolname')
        else:
            _label = __class__.__name__
        self.__name__ = _label.replace('_', '').replace('Algorithm', '').lower()
        self.__displayname__ = _label.replace('_', ' ').replace(' Algorithm', '').strip()
        self.__helpstr__ = self.__doc__
        self.__group__ = "BaseClasses"


    def initAlgorithm(self, config):
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.INPUT,
                description='Digital Elevation Model'
            ) )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.AZ,
                description="Azimuth (In degrees; clockwise from north)",
                type=QgsProcessingParameterNumber.Double,
                defaultValue=337.5,
                minValue=0,
                maxValue=360
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.EL,
                description="Elevation (In degrees above the horizon)",
                type=QgsProcessingParameterNumber.Double,
                defaultValue=45,
                minValue=0,
                maxValue=90
            )
        )
        self.addParameter(
            QgsProcessingParameterRasterDestination(
                self.OUTPUT,
                description="Output"
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
       pass

    def name(self):
        return self.__name__

    def displayName(self):
        return self.tr(self.__displayname__)

    def group(self):
        return self.tr(self.__group__)

    def groupId(self):
        return self.__group__

    def shortHelpString(self):
        return self.__helpstr__

    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def terrain_from_DEM(self, fname, elev_only=False):
        dem = gdal.Open( fname,  gdal.GA_ReadOnly)
        if dem == None:
            raise OSError("Could not load input DEM: {}".format(fname))
        srs = dem.GetSpatialRef()
        xform = dem.GetGeoTransform()
        if not srs.IsProjected():
            raise RuntimeError("Must use DEM in a projected spatial reference.")
        if dem.RasterCount != 1:
            raise RuntimeError("I expect DEM rasters made of a single band holding elevation values.")
        demData = dem.GetRasterBand(1).ReadAsArray()
        ndv = dem.GetRasterBand(1).GetNoDataValue()

        demData[demData == ndv] = 0
        if elev_only:
            terrain=xr.Dataset(
                data_vars=dict(
                    DEM=(["rows", "cols"], demData),
                ),
                attrs=dict(cellwidth=xform[1])
            )
        else:
            terrain = surface.study(demData, xform[1])
        terrain.attrs['geotransform'] = xform
        terrain.attrs['spatialreference'] = srs
        dem = None
        return terrain


    def write_terrain_layer(self, t, k, outFile):
        """
        Writes one of the xarray 'layers' to the named file

        :param outFile: Filename to write
        :param t: Terrain data structure (must be already populated)
        :param k: The key to specify which 'layer' of the terrain to export.  Example: 'SNV', 'DEM', etc.
        """
        DriverName = QgsRasterFileWriter.driverForExtension(os.path.splitext(outFile)[1])
        if not DriverName:
            DriverName = 'GTiff'
            outFile = outFile.replace('.', '_') + ".tif"
        driver = gdal.GetDriverByName(DriverName)

        try:
            x_ = t[k].data.shape[0]
            y_ = t[k].data.shape[1]
            if len(t[k].data.shape) == 3:
                z_ = t[k].data.shape[2]
            else:
                z_ = 1
        except KeyError:
            raise

        out_ds = driver.Create(outFile, y_, x_, z_, gdal.GDT_Float32)
        if out_ds == None:
            raise OSError("Could not create outfile {}".format(outFile))
        out_ds.SetGeoTransform(t.attrs['geotransform'])
        out_ds.SetProjection(t.attrs['spatialreference'].ExportToWkt())

        if z_ == 1:
            ## Single-band output
            band = out_ds.GetRasterBand(1)
            band.WriteArray(t[k].data)
        else:
            stacked_array = np.moveaxis(t[k].data, 2, 0)
            for b in range(1, z_+1):
                band = out_ds.GetRasterBand(b)
                band.WriteArray(stacked_array[b-1])
        out_ds = None


class Sky_Tool_Algorithm(Relief_Tool_Algorithm):
    OUTPUT = 'OUTPUT'
    INPUT = 'INPUT'
    VE = 'VE'
    SKY_CONFIG = 'SKY_CONFIG'

    def createInstance(self):
        return Sky_Tool_Algorithm()

    def __init__(self, **kwargs) -> None:
        super().__init__()
        if 'toolname' in kwargs:
            super().__init__(toolname=kwargs.get('toolname'))
            _label = kwargs.get('toolname')
        else:
            super().__init__(toolname=__class__.__name__)
            _label = __class__.__name__
        self.__name__ = _label.replace('_', '').replace('Algorithm', '').lower()
        self.__displayname__ = _label.replace('_', ' ').replace(' Algorithm', '').strip()
        self.__helpstr__ = self.__doc__
        self.__group__ = "BaseClasses"
        self._skyconfigdir = os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], r"SkyConfigFiles")
        self._skyconfigfiles = []


    def initAlgorithm(self, config):
        self._skyconfigfiles = self.listSkyConfigs(self._skyconfigdir)

        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.INPUT,
                description='Digital Elevation Model'
            ) )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.VE,
                description="Vertical Exaggeration",
                type=QgsProcessingParameterNumber.Double,
                defaultValue=5,
                minValue=0.1,
                maxValue=10
            )
        )
        self.addParameter(
            QgsProcessingParameterEnum(
                self.SKY_CONFIG,
                description="Sky Config File",
                defaultValue=self._skyconfigfiles[0],
                options=self._skyconfigfiles
            )
        )
        self.addParameter(
            QgsProcessingParameterRasterDestination(
                self.OUTPUT,
                description="Output"
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
       pass


    def listSkyConfigs(self, path):
        #QgsMessageLog.logMessage("Looking for SKY Config Files in {}".format(path) , 'ShaderPro', Qgis.Info)
        return sorted([f for f in os.listdir(path) if f.endswith('.txt')])


    def lightlist(self, fname):
        #QgsMessageLog.logMessage("Reading Sky Config File {}".format(fname) , 'ShaderPro', Qgis.Info)
        lts = []
        try:
            with open(fname, "r") as skyConfigFile:
                csvReadFile = csv.reader(skyConfigFile)
                for line in csvReadFile:
                    # brute force -- if this line does not have three comma-separated values, we skip it.
                    if (len(line) == 0) or (len(line) < 3):
                        continue
                    if line[0].startswith("Format"):  # special case... a comment line in the header has 2 commas in it.
                        continue
                    lts.append(line)
        except FileNotFoundError:
            QgsMessageLog.logMessage("No such file {}".format(fname) , 'ShaderPro', Qgis.Warning)
            lts = []
        except PermissionError:
            QgsMessageLog.logMessage("Cannot read {}".format(fname) , 'ShaderPro', Qgis.Warning)
            lts = []
        except IOError:
            QgsMessageLog.logMessage("IO Error on file {}".format(fname) , 'ShaderPro', Qgis.Warning)
            lts = []
        # QgsMessageLog.logMessage("Found {} lights".format(len(lts)) , 'ShaderPro', Qgis.Info)
        return lts

