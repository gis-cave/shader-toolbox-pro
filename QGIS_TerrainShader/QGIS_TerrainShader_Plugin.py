# -*- coding: utf-8 -*-

from os import path
import sys
import inspect

from qgis.core import Qgis, QgsApplication, QgsMessageLog, QgsProviderConnectionException

cmd_folder = path.split(inspect.getfile(inspect.currentframe()))[0]
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

class TerrainShaderToolsPlugin(object):
    def __init__(self):
        self.provider = None

    def initProcessing(self):
        """Init Processing provider for QGIS >= 3.8."""
        try:
            from .QGIS_TerrainShader_Provider import ShaderToolsProProvider
            self.provider = ShaderToolsProProvider()
            QgsApplication.processingRegistry().addProvider(self.provider)
            QgsMessageLog.logMessage("Successfully loaded.", 'ShaderPro', level=Qgis.Success)
        except ImportError:
            QgsMessageLog.logMessage("Unable to load plugin... ", Qgis.Critical)
            raise QgsProviderConnectionException("Cannot load Shader  plugin.  See TerrainShader tab in the log panel.")
        return True


    def initGui(self):
        self.initProcessing()

    def unload(self):
        QgsApplication.processingRegistry().removeProvider(self.provider)
