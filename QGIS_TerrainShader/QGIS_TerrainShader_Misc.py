# -*- coding: utf-8 -*-

# from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterRasterDestination,
                       QgsProcessingParameterEnum,
                       QgsProcessingParameterNumber,
                       QgsProcessingFeedback,
                       QgsProcessingException
                       )


from .QGIS_TerrainShader_BaseClasses import Relief_Tool_Algorithm

from .trf import surface, generalize, shader, utils
import xarray as xr
import numpy as np

class Slope_Shading_Algorithm(Relief_Tool_Algorithm):
    """
    Computes slope.  Output is normalized to [0..1] where:
      0 == vertical terrain
      1 == horizontal terrain
    The ramp/curve between the extremes follows the cosine of slope in degrees.
    """
    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.__helpstr__ = self.__doc__
        self.__group__ = "Surface Effects"

    def initAlgorithm(self, config):
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.INPUT,
                self.tr('Digital Elevation Model:')
            )
        )
        self.addParameter(
            QgsProcessingParameterRasterDestination(
                self.OUTPUT,
                self.tr("Slope Shade")
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        if feedback is None:
            feedback = QgsProcessingFeedback()
        dem_path = str(self.parameterAsRasterLayer(parameters, self.INPUT, context).source())
        try:
            feedback.pushInfo("Reading {} ...".format(dem_path))
            t = self.terrain_from_DEM(dem_path)
        except RuntimeError as e:
            feedback.reportError("It is pitch black; you are likely to be eaten by a grue.\n", fatalError=True)
            raise QgsProcessingException(e)

        slp = surface.slope(t, output="Normalized")
        t['Slope'] = xr.DataArray(data=slp, dims=["rows", "cols"])

        output_path = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        self.write_terrain_layer(t, 'Slope', output_path)
        return {self.OUTPUT: output_path}

    def createInstance(self):
        return self.__class__()


class TPI_Algorithm(Relief_Tool_Algorithm):
    """
    Terrain Position Index.

    Computes relative position of each pixel against the average of the neighborhood.
    Neighborhood mean calculated using the filter of specified shape and size. Smoothing is implemented using Fourier transforms, which will distort at the edges of the extent with very large smoothing kernels.

    Results are normalized to fall between [-1..1].  Negative values indicate that the pixel lies below its neighbors; positive values indicate pixel is above its neighbors.
    Best visualized with divergent color ramp with 0 being fully transparent.
    """
    SIZE = "SIZE"
    SHAPE = "SHAPE"

    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.__helpstr__ = self.__doc__
        self.__group__ = "Surface Effects"
        self._filter_shapes = ['Box', 'Circle', 'Gaussian']

    def initAlgorithm(self, config):
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.INPUT,
                self.tr('Digital Elevation Model')
            )
        )
        self.addParameter(
            QgsProcessingParameterEnum(
                self.SHAPE,
                description="Filter Shape",
                defaultValue=self._filter_shapes[0],
                options=self._filter_shapes
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.SIZE,
                description="Filter Size",
                type=QgsProcessingParameterNumber.Integer,
                defaultValue=15,
                minValue=3
            )
        )
        self.addParameter(
            QgsProcessingParameterRasterDestination(
                self.OUTPUT,
            self.tr("TPI")
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        if feedback is None:
            feedback = QgsProcessingFeedback()
        dem_path = str(self.parameterAsRasterLayer(parameters, self.INPUT, context).source())
        try:
            feedback.pushInfo("Reading {} ...".format(dem_path))
            t = self.terrain_from_DEM(dem_path, elev_only=True)
        except RuntimeError as e:
            feedback.reportError("It is pitch black; you are likely to be eaten by a grue.\n", fatalError=True)
            raise QgsProcessingException(e)

        size = int(self.parameterAsInt(parameters, self.SIZE, context ))
        shp = self._filter_shapes[self.parameterAsInt(parameters, self.SHAPE, context)]

        _tpi = t["DEM"].data - generalize.fft_smooth(t['DEM'].data, size, shape=shp)
        n = np.absolute(_tpi).max() # normalize to get range from [-1..1]
        t['TPI'] = xr.DataArray(data=_tpi/n, dims=["rows", "cols"])

        output_path = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        self.write_terrain_layer(t, 'TPI', output_path)
        return {self.OUTPUT: output_path}

    def createInstance(self):
        return self.__class__()


class Curvature_Shading_Algorithm(Relief_Tool_Algorithm):
    """

    """
    SPAN="SPAN"
    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.__helpstr__ = self.__doc__
        self.__group__ = "Surface Effects"

    def initAlgorithm(self, config):
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.INPUT,
                self.tr('Digital Elevation Model')
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.SPAN,
                description="Span",
                type=QgsProcessingParameterNumber.Integer,
                defaultValue=1,
                minValue=1,
                maxValue=10
            )
        )
        self.addParameter(
            QgsProcessingParameterRasterDestination(
                self.OUTPUT,
            self.tr("Curvature Shading")
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        if feedback is None:
            feedback = QgsProcessingFeedback()
        dem_path = str(self.parameterAsRasterLayer(parameters, self.INPUT, context).source())
        try:
            feedback.pushInfo("Reading {} ...".format(dem_path))
            t = self.terrain_from_DEM(dem_path, elev_only=True)
        except RuntimeError as e:
            feedback.reportError("It is pitch black; you are likely to be eaten by a grue.\n", fatalError=True)
            raise QgsProcessingException(e)

        _s = int(self.parameterAsInt(parameters, self.SPAN, context ))

        _k = surface.profile_curvature(t, span=_s)
        n = np.absolute(_k).max()
        t['PrC'] = xr.DataArray(data=_k/n, dims=["rows", "cols"])

        output_path = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        self.write_terrain_layer(t, 'PrC', output_path)


        return {self.OUTPUT: output_path}

    def createInstance(self):
        return self.__class__()

    # style = instance.parameterAsString(parameters, "STYLEFILE", context)
    # processed_layer = QgsProcessingUtils.mapLayerFromString(dest_id, context)
    # processed_layer.loadNamedStyle(style)
    # processed_layer.triggerRepaint()
    # feedback.pushInfo('Loaded buffer styling.')

class Aspect_Weighted_Slope_Shading_Algorithm(Relief_Tool_Algorithm):
    """

    """
    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.__helpstr__ = self.__doc__
        self.__group__ = "Surface Effects"

    def initAlgorithm(self, config):
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.INPUT,
                self.tr('Digital Elevation Model:')
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.AZ,
                description="Azimuth (In degrees; clockwise from north)",
                type=QgsProcessingParameterNumber.Double,
                defaultValue=337.5,
                minValue=0,
                maxValue=360
            )
        )
        self.addParameter(
            QgsProcessingParameterRasterDestination(
                self.OUTPUT,
                self.tr("Aspect-Weighted Slope")
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        if feedback is None:
            feedback = QgsProcessingFeedback()
        dem_path = str(self.parameterAsRasterLayer(parameters, self.INPUT, context).source())
        try:
            feedback.pushInfo("Reading {} ...".format(dem_path))
            t = self.terrain_from_DEM(dem_path)
        except RuntimeError as e:
            feedback.reportError("It is pitch black; you are likely to be eaten by a grue.\n", fatalError=True)
            raise QgsProcessingException(e)

        az = float(self.parameterAsDouble(parameters, self.AZ, context ))

        weights = shader.aspect_shading(t, utils.lightVector(az, 0))
        # weights are 0 (typically black) for areas facing away from light; 1 (white) for areas facing the light.
        # We need that reversed for the multiplication step below (0=white  :=  multiply by zero to remove/reduce)
        w = 1-weights
        slp = t['SNV'].sel(component="Nz").data
        # Same deal with slopes -- we need darkly shaded areas to have high value so that white areas multiply by zero
        s = 1-slp
        results = 1 - (w * s) # Need to undo, so that 0=dark again
        results[np.isnan(results)] = 1
        t['SlopeShade'] = xr.DataArray(data=results, dims=["rows", "cols"])

        output_path = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        self.write_terrain_layer(t, 'SlopeShade', output_path)
        return {self.OUTPUT: output_path}

    def createInstance(self):
        return self.__class__()
