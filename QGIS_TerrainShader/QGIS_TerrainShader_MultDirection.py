# -*- coding: utf-8 -*-

# from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (
                QgsProcessingParameterRasterLayer,
                QgsProcessingParameterRasterDestination,
                QgsProcessingParameterNumber,
                QgsProcessingParameterEnum,
                QgsProcessingFeedback,
                QgsProcessingException
                )

from .QGIS_TerrainShader_BaseClasses import Relief_Tool_Algorithm, Sky_Tool_Algorithm

from .trf import shader, utils, generalize, surface
import xarray as xr
import os
import numpy as np

class MDOW_Algorithm(Relief_Tool_Algorithm):
    """
    Multi-Direction Oblique Weighted Hillshade with configurable light source.

    Implementation of Mark(1992) MDOW algorithm.
    As published, this algorithm computes hillshades based only on a pre-configured dominant light direction.
    This tool permits light direction to be configured. Accept the default of 295.5 to compute the 'Classic' MDOW.

    See: Mark, R. (1992). Multidirectional, oblique-weighted, shaded-relief image of the Island of Hawaii (OF-92-422). US Gological Survey.
    """
    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.__helpstr__ = self.__doc__
        self.__group__ = "Multi-Direction"

    def initAlgorithm(self, config):
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.INPUT,
                description='Digital Elevation Model'
            ) )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.AZ,
                description="Azimuth of dominant light direction:",
                type=QgsProcessingParameterNumber.Double,
                defaultValue=295.5,
                minValue=0,
                maxValue=360
            )
        )
        self.addParameter(
            QgsProcessingParameterRasterDestination(
                self.OUTPUT,
                description="MDOW Shaded Relief"
            )
        )


    def processAlgorithm(self, parameters, context, feedback):
        if feedback is None:
            feedback = QgsProcessingFeedback()
        try:
            dem_path = str(self.parameterAsRasterLayer(parameters, self.INPUT, context).source())
            feedback.pushInfo("Reading {} ...".format(dem_path))
            t = self.terrain_from_DEM(dem_path)
        except RuntimeError as e:
            feedback.reportError("It is pitch black; you are likely to be eaten by a grue.\n", fatalError=True)
            raise QgsProcessingException(e)

        az = float(self.parameterAsDouble(parameters, self.AZ, context ))

        feedback.setProgress(5)

        _center=az
        _step=22.5

        feedback.setProgressText("Computing Four Hillshades {} ...")
        shade225 = shader.clamp(shader.lambert(t, utils.lightVector(_center - 3*_step, 30)))
        shade270 = shader.clamp(shader.lambert(t, utils.lightVector(_center - _step, 30)))
        shade315 = shader.clamp(shader.lambert(t, utils.lightVector(_center + _step, 30)))
        shade360 = shader.clamp(shader.lambert(t, utils.lightVector(_center + 3*_step, 30)))
        feedback.setProgress(20)

        feedback.setProgressText("Smoothing terrain ...")
        h03 = generalize.fft_smooth(t['DEM'].data, 30, shape="Circle")
        feedback.setProgress(40)

        feedback.setProgressText("Computing aspect of smoothed terrain ...")
        c = t.attrs['geotransform'][1]
        h03_t = surface.study(h03, c)
        asp = surface.aspect(h03_t)
        asp[asp < 0] = 293
        feedback.setProgress(60)

        feedback.setProgressText("Computing cell weights ...")
        w225 = np.sin( (asp - (_center - 3*_step)) * (np.pi / 180) )**2
        w270 = np.sin( (asp - (_center - 1*_step)) * (np.pi / 180) )**2
        w315 = np.sin( (asp - (_center + _step)) * (np.pi / 180) )**2
        w360 = np.sin( (asp - (_center + 3*_step)) * (np.pi / 180) )**2
        feedback.setProgress(80)

        feedback.setProgressText("Building weighted hillshade ...")
        hs = (w225*shade225  + w270*shade270 + w315*shade315 + w360*shade360) / 2
        t['HS'] = xr.DataArray(data=hs, dims=["rows", "cols"])
        feedback.setProgress(100)

        output_path = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        feedback.setProgressText("Writing {} ...".format(os.path.basename(output_path)))
        self.write_terrain_layer(t, 'HS', output_path)
        return {self.OUTPUT: output_path}

    def createInstance(self):
        return self.__class__()



class Sky_Model_Algorithm(Sky_Tool_Algorithm):
    """
    This is a variation of Kennelly & Stewart's sky modeling technique.  It has been adapted to permit the configuration of how each consituent shading layer is composed (shade only, shadow only, or both).
    Add your own sky configurations to the SkyConfigFiles folder inside the plugin folder.
    See the <a href='https://gitlab.com/gis-cave/shader-toolbox-qgis/-/wikis/Tools/Multi-Direction/SkyModel'>Online Documentation</a> for details.
    """
    LAYER_COMBO = "LAYER_COMBO"

    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.__helpstr__ = self.__doc__
        self.__group__ = "Multi-Direction"
        self._layercombo = ['Shade & Shadow', 'Shade Only', 'Shadow Only']

    def initAlgorithm(self, config):
        super().initAlgorithm(config)
        self.addParameter(
            QgsProcessingParameterEnum(
                self.LAYER_COMBO,
                description="Per-Hillshade Combination:",
                defaultValue=self._layercombo[0],
                options=self._layercombo
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        if feedback is None:
            feedback = QgsProcessingFeedback()
        try:
            dem_path = str(self.parameterAsRasterLayer(parameters, self.INPUT, context).source())
            feedback.pushInfo("Reading {} ...".format(dem_path))
            t = self.terrain_from_DEM(dem_path)
        except RuntimeError as e:
            feedback.reportError("It is pitch black; you are likely to be eaten by a grue.\n", fatalError=True)
            raise QgsProcessingException(e)

        # Scale the DEM by the vertical exaggeration...
        # Note that this will affect shadows, but not the shading, as that depends on surface normal vectors already computed at this point.
        ve = float(self.parameterAsDouble(parameters, self.VE, context ))
        t['DEM'].data *= ve

        sk_fname = self._skyconfigfiles[self.parameterAsInt(parameters, self.SKY_CONFIG, context)]
        sk_fname = os.path.join(self._skyconfigdir, sk_fname)
        lights = self.lightlist(sk_fname)
        feedback.setProgressText("Processing {} lights from {}...".format(len(lights), os.path.basename(sk_fname)))

        m = int(self.parameterAsString(parameters, self.LAYER_COMBO, context))
        # m is an index into self._layercombo

        accumulator = np.zeros(t['DEM'].data.shape)
        for (i, (azimuth, elev, wt)) in enumerate(lights):
            p = 100.00 * ((i+1) / len(lights))
            feedback.setProgress(int(p))
            if feedback.isCanceled():
                break

            if m == 0 : # "Shade & Shadow"
                shadowArray = shader.shadowLine(t, float(azimuth), float(elev))
                shadeArray = shader.lambert(t, utils.lightVector(float(azimuth), float(elev)))
                shadeArray[shadeArray<0] = 0
                shadeArray[shadowArray>0] = 0
            if m == 1: # "ShadeOnly":
                shadeArray = shader.lambert(t, utils.lightVector(float(azimuth), float(elev)))
                shadeArray[shadeArray<0] = 0
            if m == 2: # "ShadowOnly":
                shadowArray = shader.shadowLine(t, float(azimuth), float(elev))
                shadowArray[shadowArray > 0] = 1
                shadeArray = 1 - shadowArray
            accumulator = accumulator + (float(wt) * shadeArray)

        t['HS'] = xr.DataArray(data=accumulator, dims=["rows", "cols"])
        output_path = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        feedback.setProgressText("Writing {} ...".format(os.path.basename(output_path)))
        self.write_terrain_layer(t, 'HS', output_path)
        return {self.OUTPUT: output_path}

    def createInstance(self):
        return self.__class__()