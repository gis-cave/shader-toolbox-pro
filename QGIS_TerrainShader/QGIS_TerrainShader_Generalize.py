# -*- coding: utf-8 -*-

#from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterRasterDestination,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterEnum,
                       QgsProcessingFeedback,
                       QgsProcessingException
                        )


from .QGIS_TerrainShader_BaseClasses import Relief_Tool_Algorithm

from .trf import shader, generalize
import xarray as xr
import numpy as np

class ROF_Denoise_Algorithm(Relief_Tool_Algorithm):
    """
    """
    WT = "WT"

    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.__helpstr__ = self.__doc__
        self.__group__ = "Generalize"

    def initAlgorithm(self, config):
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.INPUT,
                self.tr('Digital Elevation Model')
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.WT,
                description="Weight",
                type=QgsProcessingParameterNumber.Integer,
                defaultValue=20,
                minValue=1
            )
        )
        self.addParameter(
            QgsProcessingParameterRasterDestination(
                self.OUTPUT,
            self.tr("Output")
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        if feedback is None:
            feedback = QgsProcessingFeedback()
        try:
            dem_path = str(self.parameterAsRasterLayer(parameters, self.INPUT, context).source())
            feedback.pushInfo("Reading {} ...".format(dem_path))
            # t = terrain_from_DEM(dem_path)
            t = self.terrain_from_DEM(dem_path, elev_only=True)
        except RuntimeError as e:
            feedback.reportError("It is pitch black; you are likely to be eaten by a grue.\n", fatalError=True)
            raise QgsProcessingException(e)

        wt = int(self.parameterAsInt(parameters, self.WT, context ))
        (denoised_dem, _ ) = generalize.rof_denoise(t['DEM'].data, tv_weight=wt)
        t['smooth'] = xr.DataArray(data=denoised_dem, dims=["rows", "cols"])

        output_path = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        self.write_terrain_layer(t, 'smooth', output_path)
        return {self.OUTPUT: output_path}

    def createInstance(self):
        return self.__class__()

class FFT_Smooth_Algorithm(Relief_Tool_Algorithm):
    """
    Runs a box/circle/gaussian kernel across the DEM.
    """
    SIZE = "SIZE"
    SHAPE = "SHAPE"

    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.__helpstr__ = self.__doc__
        self.__group__ = "Generalize"
        self._smooth_shapes = ['Box', 'Circle', 'Gaussian']

    def initAlgorithm(self, config):
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.INPUT,
                self.tr('Digital Elevation Model')
            )
        )
        self.addParameter(
            QgsProcessingParameterEnum(
                self.SHAPE,
                description="Filter Shape",
                defaultValue=self._smooth_shapes[0],
                options=self._smooth_shapes
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.SIZE,
                description="Size (in pixels)",
                type=QgsProcessingParameterNumber.Integer,
                defaultValue=9,
                minValue=3
            )
        )
        self.addParameter(
            QgsProcessingParameterRasterDestination(
                self.OUTPUT,
            self.tr("Output")
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        if feedback is None:
            feedback = QgsProcessingFeedback()
        try:
            dem_path = str(self.parameterAsRasterLayer(parameters, self.INPUT, context).source())
            feedback.pushInfo("Reading {} ...".format(dem_path))
            t = self.terrain_from_DEM(dem_path, elev_only=True)
        except RuntimeError as e:
            feedback.reportError("It is pitch black; you are likely to be eaten by a grue.\n", fatalError=True)
            raise QgsProcessingException(e)

        size = int(self.parameterAsInt(parameters, self.SIZE, context ))
        shp = self._smooth_shapes[self.parameterAsInt(parameters, self.SHAPE, context)]

        t['smooth'] = xr.DataArray(data=generalize.fft_smooth(t['DEM'].data, size, shape=shp), dims=["rows", "cols"])

        output_path = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        self.write_terrain_layer(t, 'smooth', output_path)
        return {self.OUTPUT: output_path}

    def createInstance(self):
        return self.__class__()

