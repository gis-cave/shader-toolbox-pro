# -*- coding: utf-8 -*-

#from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterRasterDestination,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterEnum,
                       QgsProcessingFeedback,
                       QgsProcessingException
                        )


from .QGIS_TerrainShader_BaseClasses import Relief_Tool_Algorithm

from .trf import shader, generalize
import xarray as xr
import numpy as np

class Simple_Shadows_Algorithm(Relief_Tool_Algorithm):
    """
    Casts shadows over the specified terrain. 1 for enshadowed pixels, NoData for unshadowed pixels.
    """
    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.__helpstr__ = self.__doc__
        self.__group__ = "Primitives"

    def processAlgorithm(self, parameters, context, feedback):
        if feedback is None:
            feedback = QgsProcessingFeedback()
        try:
            dem_path = str(self.parameterAsRasterLayer(parameters, self.INPUT, context).source())
            feedback.pushInfo("Reading {} ...".format(dem_path))
            t = self.terrain_from_DEM(dem_path, elev_only=True)
        except RuntimeError as e:
            feedback.reportError("It is pitch black; you are likely to be eaten by a grue.\n", fatalError=True)
            raise QgsProcessingException(e)

        az = float(self.parameterAsDouble(parameters, self.AZ, context ))
        el = float(self.parameterAsDouble(parameters, self.EL, context ))
        shadowArray = shader.shadowLine(t, az, el)
        shadowArray[shadowArray > 0] = 1
        shadowArray[shadowArray == 0] = None

        t['Shadow'] = xr.DataArray(data=shadowArray, dims=["rows", "cols"])

        output_path = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        self.write_terrain_layer(t, 'Shadow', output_path)
        return {self.OUTPUT: output_path}

    def createInstance(self):
        return self.__class__()


class Surface_Normal_Vectors_Algorithm(Relief_Tool_Algorithm):
    """
    Generates 3-band raster representing x, y, and z components of surface normal vectors.
    Pixel values in each band lie between [-1.0 .. 1.0]
    """
    def __init__(self):
        super().__init__(toolname=__class__.__name__)
        self.__helpstr__ = self.__doc__
        self.__group__ = "Primitives"

    def initAlgorithm(self, config):
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.INPUT,
                self.tr('Digital Elevation Model')
            ) )
        self.addParameter(
            QgsProcessingParameterRasterDestination(
                self.OUTPUT,
            self.tr("Surface Normal Vectors")))

    def processAlgorithm(self, parameters, context, feedback):
        if feedback is None:
            feedback = QgsProcessingFeedback()
        try:
            dem_path = str(self.parameterAsRasterLayer(parameters, self.INPUT, context).source())
            feedback.pushInfo("Reading {} ...".format(dem_path))
            t = self.terrain_from_DEM(dem_path)
        except RuntimeError as e:
            feedback.reportError("It is pitch black; you are likely to be eaten by a grue.\n", fatalError=True)
            raise QgsProcessingException(e)

        output_path = self.parameterAsOutputLayer(parameters, self.OUTPUT, context)
        self.write_terrain_layer(t, 'SNV', output_path)
        return {self.OUTPUT: output_path}

    def createInstance(self):
        return self.__class__()