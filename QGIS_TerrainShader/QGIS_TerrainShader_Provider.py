# -*- coding: utf-8 -*-

import os
import sys
import inspect

from qgis.core import QgsApplication, QgsProcessingProvider


from .QGIS_TerrainShader_Primitives import Simple_Shadows_Algorithm, Surface_Normal_Vectors_Algorithm

from .QGIS_TerrainShader_Generalize import FFT_Smooth_Algorithm, ROF_Denoise_Algorithm
from .QGIS_TerrainShader_ShadedRelief import Lambertian_Shader_Algorithm, LommelSeeliger_Shader_Algorithm, OrenNayer_Shader_Algorithm, Minnaerts_Reflectance_Algorithm
from .QGIS_TerrainShader_MultDirection import MDOW_Algorithm, Sky_Model_Algorithm
from .QGIS_TerrainShader_Misc import Slope_Shading_Algorithm, TPI_Algorithm, Curvature_Shading_Algorithm, Aspect_Weighted_Slope_Shading_Algorithm


class ShaderToolsProProvider(QgsProcessingProvider):
    # def __init__(self):
    #     """
    #     Default constructor.
    #     """
    #     super().__init__(self)

    def unload(self):
        pass

    def loadAlgorithms(self):
        """
        Loads all algorithms belonging to this provider.
        """
        # Primitives:
        self.addAlgorithm(Surface_Normal_Vectors_Algorithm())
        self.addAlgorithm(Simple_Shadows_Algorithm())


        #generalize
        self.addAlgorithm(FFT_Smooth_Algorithm())
        self.addAlgorithm(ROF_Denoise_Algorithm())

        # Shaded Relief
        self.addAlgorithm(Lambertian_Shader_Algorithm())
        self.addAlgorithm(LommelSeeliger_Shader_Algorithm())
        self.addAlgorithm(OrenNayer_Shader_Algorithm())
        self.addAlgorithm(Minnaerts_Reflectance_Algorithm())

        # Multi-directional shading techniques
        self.addAlgorithm(MDOW_Algorithm())
        self.addAlgorithm(Sky_Model_Algorithm())

        # Misc / Everything Else
        self.addAlgorithm(Slope_Shading_Algorithm())
        self.addAlgorithm(TPI_Algorithm())
        self.addAlgorithm(Curvature_Shading_Algorithm())
        self.addAlgorithm(Aspect_Weighted_Slope_Shading_Algorithm())



    def id(self):
        return 'ShaderPro'

    def name(self):
        return self.tr('Shader Tools for QGIS')

    def icon(self):
        """
        Should return a QIcon which is used for your provider inside
        the Processing toolbox.
        """
        return QgsProcessingProvider.icon(self)

    def longName(self):
        return self.name()
