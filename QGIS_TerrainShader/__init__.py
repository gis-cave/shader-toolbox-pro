# -*- coding: utf-8 -*-

from qgis.core import Qgis, QgsMessageLog, QgsProviderConnectionException
from qgis.utils import iface

try:
    import xarray as xr
except ImportError:
    QgsMessageLog.logMessage("Cannot Load Plugin.  Ensure you have added xarray to your QGIS Python environment.", 'ShaderPro', level=Qgis.Critical)
    QgsMessageLog.logMessage("Debugging Steps --> Open the python console and attempt to 'import xarray'.", 'ShaderPro', level=Qgis.Info)
    QgsMessageLog.logMessage("You will likely need to use pip or the OSGeo4W shell to install this dependency", 'ShaderPro', level=Qgis.Info)
    iface.messageBar().pushMessage(
        "ShaderPro", "Ooops! Could not load the plugin.  Is xarray installed?", Qgis.Critical, 10
    )
    raise QgsProviderConnectionException("__ Cannot load Shader Pro plugin.__  See 'ShaderPro' tab in the log panel.")


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """
    Load from file
    """
    #
    from .QGIS_TerrainShader_Plugin import TerrainShaderToolsPlugin
    return TerrainShaderToolsPlugin()
